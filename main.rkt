#lang racket

(require web-server/servlet
         web-server/servlet-env)

(provide/contract (start (request? . -> . response?)))

(struct post
  (name
   content
   id
   post_time)
  #:mutable)

(struct thread
  (posts id topic)
  #:mutable)

(struct board
  (threads name)
  #:mutable)


(define server-dir (current-directory))
(define last-num -1)

(define /b/
  (board (list ;; The diffrent threads in a board
          (thread (list ;; The diffrent posts in the thread
                   (post "Anomymous"
                         "Hello this is a test"
                         1
                         "04:20")
                   (post "Anomymous2"
                         "Hello this is a test"
                         2
                         "04:22"))
                  1
                  "testing")
          (thread (list ;; The diffrent posts in the thread
                   (post "Anomymous1"
                         "Hello this is a test1"
                         1
                         "04:21")
                   (post "Anomymous2"
                         "Hello this is a test"
                         2
                         "04:22"))
                  2
                  "testing1")
          )
         "/b/"))

;; RENDER ENTIRE BOARD B
;; board -> quoted xexpr
;; Will return the quoted board (if it exists), with all threads (and boards) rendered.
;; TODO: Add error-handeling, Also ability to only quote the OP-post and last threads,
;; also some modes where only OP-posts are quoted, also some kind of list-mode?
(define (render-board B)
  (quasiquote (div ((class "board"))
                   (unquote-splicing (map render-thread-as-list (board-threads B))))))


;; Given the id of a existing thread, return the thread-object.
;; board, thread-id, function -> thread
(define (get-thread-by-id board thread_number f)
  "Get or apply a function on a given thread"
  (let ((answer ;; Either '() if out of bounds or '(thing i want)
         (filter (lambda (x) (or (number? x) (string? x) (thread? x)))
                 (map (lambda (current_thread)
                        (if (equal?  (thread-id current_thread) thread_number)
                            (f current_thread)
                            '()))
                      (board-threads board)))))
    (if (null? answer) #f (car answer))))


;; path to stylesheet
(define style-path "/style_master.css")
;; load local-path as a stylesheet
(define (static-stylesheet local-path) ;; Placed in head tag
  (quasiquote
   (link ((rel "stylesheet")
          (href (unquote local-path))
          (type "text/css")))))



;; Given the id of the thread of the post and the id of the post, the
;; post is 
(define (get-post-by-id board thread_id post_id f)
  "Get or apply a function on a given post"
  (let ((answer ;; Either '() if out of bounds or '(thing i want)
         (filter (lambda (x) (or (number? x) (string? x) (post? x) (thread? x)))
                (map (lambda (current_post)
                       (if (equal? (post-id current_post) post_id)
                           (f current_post)
                           '()))
                     (thread-posts (get-thread-by-id board thread_id (lambda (x) x)))))))
     (if (null? answer) #f (car answer))))

(define (render-thread-as-list T . option)
  ;; STM = Single Thread Mode
  (quasiquote (div ((class "thread"))
                   (table ((class "thread-header")) ;; Thread
                          (tr (td ((class "thread-topic"))          (unquote (thread-topic T)))
                              (td ((class "thread-id")) " id: "     (unquote (number->string (thread-id T))))
                              (unquote (if (null? option) ;; Browing all threads
                                           (quasiquote (td ((class "thread-link"))
                                                           (a ((href (unquote (string-append "/forum/thread/"
                                                                                             (number->string (thread-id T))))))
                                                              "[view]")))
                                           
                                           (quasiquote (td ((class "thread-link"))
                                                           (a ((href "/forum/")) "[Return]"))))))) ;; Viewing in STM
                              
                   (div ((class "thread-posts")) ;; Posts 
                        (ul (unquote-splicing (map (curryr render-post-as-list-item (thread-id T) "thread-view")
                                                   (thread-posts T)))))
                                   
                   (div ((class "thread-reply")) ;; Replyfield
                        (p ((class "thread-reply-message")) "Reply to thread")
                        (unquote (render-post-form T)))
                   (br))))

(define (render-post-as-list-item P . option)
  (if (empty? P) (quote (p "Empty thread"))
      (quasiquote (div ((class "post-whole"))
                   (table ((class "post-main"))
                              (tr (td ((class "post-name"))
                                      "Name: " (unquote (post-name P)))
                                  (td ((class "post-id"))
                                      "No."(unquote (number->string (post-id P))))
                                  ;;(td ((class "post-time"))
                                  ;;    "STime: "(unquote (post-post_time P)))
                                  (unquote (if (> (length option) 1)
                                               (quasiquote (td (a ((href (unquote (string-append "/forum/thread/"
                                                                                                 (number->string (car option))
                                                                                                 "/post/"
                                                                                                 (number->string (post-id P))))))
                                                                  " [view]")))
                                               (quasiquote (td (a ((href (unquote (string-append "/forum/thread/"
                                                                                                 (number->string (car option))))))
                                                                  "[Return]"))))))

                              (tr (td (blockquote ((class "post-content"))
                                                  (unquote (post-content P))))))
                       (br)))))
;; Creating a post ;;
(define (render-post-form T)
  (quasiquote
   (form
    (table ((class "post-form"))
           (tr (td (input ((value "Anonymous")
                           (name (unquote (string-append "name:"
                                                         (number->string (thread-id T)))))))))
           (tr (td (textarea ((value "content")
                              (rows "5")
                              (cols  "26")
                              (name (unquote (string-append "content:"
                                                            (number->string (thread-id T)))))))))
           (tr (td (input ((type "submit")))))))))

(define (can-parse-post? bindings thread-number)
  (and (exists-binding? (string->symbol (string-append "name:"  (number->string thread-number))) bindings)
       (exists-binding? (string->symbol (string-append "content:"  (number->string thread-number))) bindings)))

(define (parse-post bindings thread-number)
  (post (extract-binding/single (string->symbol (string-append "name:"  (number->string thread-number))) bindings)
        (extract-binding/single (string->symbol (string-append "content:" (number->string thread-number))) bindings)
        69
        "13:37"))

(define (modify-single-thread used-board thread-number function)
  (map (lambda (T) (if (= (thread-id T) thread-number)
                       (function T)
                       T))
        (board-threads used-board)))

(define (insert-post! used-board thread-number new-post)
  (set-board-threads! used-board
                      (modify-single-thread used-board
                                            thread-number
                                            (lambda (T) ;; Append a given post to the thread: Thread -> Thread
                                              (thread (append (thread-posts T) (list new-post))
                                                      (thread-id T)
                                                      (thread-topic T))))))

                       
;; Creating a thread ;;
(define (render-thread-form)
  (quasiquote
   (form (input ((name "topic")))
         (input ((type "submit"))))))

(define (can-parse-thread? bindings)
  (exists-binding? 'topic bindings))

;; Creates the thread object. 
(define (parse-thread bindings board)
  (thread '()
          (+ 1 (length (board-threads board)))
          (extract-binding/single 'topic bindings)))

;; board, quoted thread -> None
(define (insert-thread! used-board new-thread)
  (set-board-threads! used-board
                     (append (board-threads used-board)
                             (list new-thread))))

;; board, request -> response/xexpr
(define (render-entire-page chosen-board request)
  (response/xexpr
   (quasiquote
    (html (head (title "testchan")
                (unquote (static-stylesheet style-path)))
          (body (unquote (render-board chosen-board))
                (h6 "Create a new thread-topic!:")
                (unquote (render-thread-form))))))
  )

;; bindings -> number
(define (extract-thread-number bindings)
  (let* ((final-number 1)
         (n (map (lambda (symbol)
                   (let ((id? (cdr (string-split (symbol->string (car symbol)) ":"))))
                     (cond ((number? id?) (set! final-number id?)))))
                 (car (list bindings)))))
    final-number))

(define (render-single-thread request board thread-id)
  (let ((TID (if (number? thread-id) thread-id (string->number thread-id))))
        (response/xexpr
         (quasiquote  ;; Title format ex: Thread Nr.1
          (html (head (title (unquote (string-append "Thread Nr."
                                                     (number->string TID))))
                      (unquote (static-stylesheet style-path)))
                (body (unquote (render-thread-as-list (get-thread-by-id 
                                                                board
                                                                TID
                                                                (lambda (x) x))
                                                      "Single mode"))))))))

(define (render-single-post request board thread-id post-id)
  (let ((TID (if (number? thread-id) thread-id (string->number thread-id)))
        (PID (if (number? post-id) post-id (string->number post-id))))
        (response/xexpr
         (quasiquote  ;; Title format ex: Thread Nr.1
          (html (head (title (unquote (string-append "Post Nr."
                                                     (number->string PID))))
                      (unquote (static-stylesheet style-path)))
                (body (unquote (render-post-as-list-item (get-post-by-id 
                                                          board
                                                          TID
                                                          PID
                                                          (lambda (x) x))
                                                         (string->number thread-id)))))))))



(define (handle-single-thread request thread-id)
  (let ((bindings (request-bindings request))
        (thread-id (string->number thread-id)))              ;; Extract bindings.
         (if (can-parse-post? bindings thread-id)            ;; User has posted a Reply?
             (begin (set! last-num thread-id) ;; Set last posted thread and Upload Reply.
                    (insert-post! /b/ thread-id (parse-post bindings thread-id))
                    (redirect (string-append "/forum/thread/" (number->string thread-id))))
             (render-single-thread request /b/ thread-id)))) ;; User didn't post Reply.

(define (handle-single-post request thread-id post-id)
  (render-single-post request /b/ thread-id post-id))


(define-values (dispatch-thread request)
  (dispatch-rules
   ;; Show ordinary page with all threads and replyforms.
   ;; if the url looks like "/forum"
   [("forum")
    request-handler]
   ;; Show a single thread if the url
   ;; looks like "/forum/thread/<number>
   [("forum" "thread" (string-arg))
    handle-single-thread]
   ;; Show a single post if the url looks like
   ;; "/forum/thread/<number>/post/<number>"
   [("forum" "thread" (string-arg) "post" (string-arg))
    handle-single-post]
   [else request-handler]))

(define (start request) ;; -> None
  ;; The start function.
  ;; Check the URL and determine what
  ;; to do with what url. Like:
  ;; Display a single thread, single post ...
  (dispatch-thread request))

(define (extract-id bindings) ;; -> Positive number [Error: Negative number]
  (if (null? bindings) -1 ;; Error value, this could use a -1 thread
                          ;; for error value.
      (string->number
       (car (cdr ;; Split the number from the name.
                 ;; ex: example:69 -> 69
             (string-split (symbol->string (car (car bindings))) ":")))))) 



(define (redirect redirect-adress) ;; -> xexpr
  ;; Automatically redirect to a safe page
  ;; after Upload to avoid double upload bug
  (response/xexpr
   (quasiquote
    ;; Automatic refresh
    (html (head (meta ((http-equiv "refresh")
                       (content (unquote (string-append "0; url=" redirect-adress))))))
          ;; Manual refresh
          (body (a ((href (unquote redirect-adress)))
                   "Please click here to return!"))))))

(define (request-handler request) ;; -> None 
  ;; Handle the Uploads and Downloads
  ;; for the frontpage (all threads (expanded) + replyforms)
  (let* ((bindings (request-bindings request))
         (thread-number (if (and (not (can-parse-thread? bindings)) ;; No topic exists.
                                 (not (null? bindings)))            ;; Something was uploaded.
                            (extract-id bindings)                   ;; Get the thread id from uploaded post-form.
                            1)))
    
    (cond ((can-parse-thread? bindings)
           ;; Checks if the bindings exists.
           ;; Then append the Thread to the provided board.
           ;; After, redirect the user to a new page to prevent
           ;; double posting.
           (begin (insert-thread! /b/ (parse-thread bindings /b/))
                  (redirect "/forum")))
          
          ((can-parse-post? bindings thread-number)
           ;; Checks if the bindings exists.
           ;; Then append the Post to the provided Thread/Board.
           ;; After, redirect the user to a new page to prevent
           ;; double posting.
           (begin (set! last-num thread-number)
                  (insert-post! /b/ thread-number (parse-post bindings thread-number))
                  (redirect "/forum")))
          ;; If no detected atempt to post, assume that the
          ;; user wants to view the page.
          (else (render-entire-page /b/ request)))))
  
(define (run) ;; -> None
  ;; Run the server from the start function
  (serve/servlet start
                 #:launch-browser?   #f
                 #:quit?             #f
                 #:listen-ip         #f
                 #:port              8000
                 #:servlet-regexp    #rx"forum"
                 #:server-root-path  (build-path server-dir "server-config")
                 #:extra-files-paths (list (build-path server-dir "public-files/static"))
                 #:servlets-root     (build-path server-dir "public-files/servlets")
                 ))
 
